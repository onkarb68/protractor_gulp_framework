const{Given,When,Then}= require ('cucumber');
const { browser, element, by } = require('protractor');
const { protractor } = require('protractor/built/ptor');
const chai=require('chai');
const expect=chai.expect

         Given('I am logged to website', async ()=> {
            // Write code here that turns the phrase above into concrete actions
            await  browser.driver.get('https://angular.io/');
            
          });

 
          When('I validate the page title', async ()=> {
            // Write code here that turns the phrase above into concrete actions
            console.log('title of the page is:  '+ await browser.driver.getTitle());
            var title= await browser.driver.getTitle();
            expect(title).equal("Angular");
           
          });
 