exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    SELENIUM_PROMISE_MANAGER: false,
    defaultTimeoutInterval: 25000,
    getPageTimeout: 60000,
    allScriptsTimeout: 500000,
    framework: 'custom',
  
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    capabilities: {
        'browserName': 'chrome',
    },

 
    specs: [
        'features/*.feature'
    ],
 
    baseUrl: "https://www.linkedin.com/home/?originalSubdomain=in",
 
    cucumberOpts: {
        args: ['--window-size=800,600'],
        require: ['step_definitions/*.js','support/env.js'],
        profile: false,
        'no-source': true

    },

    onPrepare: function() {
        browser.driver.manage().window().maximize();
        return browser.driver.get("https://www.linkedin.com/home/?originalSubdomain=in");
 
  
      }
};