const { element, browser } = require("protractor");

const homePageLocators={
  searchBar:'input[title="Search"]',
  searchBarClick:'div[class="FPdoLc tfB0Bf"] input[value="Google Search"]',
  resultPage:'a[href="https://en.wikipedia.org/wiki/Mark_Zuckerberg"]'
};
exports.homePageLocators=homePageLocators;

async function enterSearch(city) {
  console.log("data555+  " +city)
  var ele=await element(by.css('input[title="Search"]'));
  ele.click();
  ele.sendKeys(city);
  browser.sleep(5000);
}

async function clickSearch() {
  await element(by.css(homePageLocators.searchBarClick)).click();
}

exports.enterSearch = enterSearch;
exports.clickSearch = clickSearch;